import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:band_names/pages/home_page.dart';
import 'package:band_names/pages/server_status_page.dart';
import 'package:band_names/services/socket_service.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => SocketService()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Band Names',
        initialRoute: HomePage.route,
        routes: {
          HomePage.route: (_) => const HomePage(),
          ServerStatusPage.route: (_) => const ServerStatusPage(),
        },
      ),
    );
  }
}
