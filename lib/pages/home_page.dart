import 'dart:io';

import 'package:band_names/models/band.dart';
import 'package:band_names/services/socket_service.dart';
import 'package:band_names/widgets/graph_bands.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  static const String route = '/';

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Band> bands = [];

  @override
  void initState() {
    final _socketService = Provider.of<SocketService>(context, listen: false);
    _socketService.socket.on('active-bands', _onActiveBands);

    super.initState();
  }

  @override
  void dispose() {
    final _socketService = Provider.of<SocketService>(context, listen: false);
    _socketService.socket.off('active-bands');
    super.dispose();
  }

  void _onActiveBands(dynamic payload) {
    bands = Band.getBandsFromMap(payload['bands'] as List<dynamic>);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final _socketService = Provider.of<SocketService>(context);

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Band Names',
          style: TextStyle(color: Colors.black87),
        ),
        centerTitle: false,
        backgroundColor: Colors.white,
        elevation: 1.0,
        actions: [
          Container(
            padding: const EdgeInsets.only(right: 10.0),
            child: _socketService.serverStatus == ServerStatus.online
                ? Icon(Icons.check_circle, color: Colors.blue[300])
                : Icon(Icons.wifi_off_outlined, color: Colors.red[300]),
          ),
        ],
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: GraphBands(bands),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: bands.length,
              itemBuilder: (_, index) => _bandTile(bands[index]),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        elevation: 1.0,
        child: const Icon(Icons.add),
        onPressed: addNewBandDialog,
      ),
    );
  }

  Widget _bandTile(Band band) {
    final _socketService = Provider.of<SocketService>(context, listen: false);

    return Dismissible(
      key: Key("${band.id}"),
      direction: DismissDirection.startToEnd,
      onDismissed: (_) => _socketService.emit('delete-band', {'id': band.id}),
      background: Container(
        padding: const EdgeInsets.only(left: 20.0),
        color: Colors.red,
        alignment: Alignment.centerLeft,
        child: const Text(
          'Delete Band',
          style: TextStyle(color: Colors.white),
        ),
      ),
      child: ListTile(
        leading: CircleAvatar(
          child: Text("${band.name}".substring(0, 2).toUpperCase()),
          backgroundColor: Colors.blue[100],
        ),
        title: Text("${band.name}"),
        trailing: Text(
          "${band.votes}",
          style: const TextStyle(fontSize: 20.0),
        ),
        onTap: () => _socketService.emit('vote-band', {'id': band.id}),
      ),
    );
  }

  void addNewBandDialog() {
    final textEditingController = TextEditingController();

    if (Platform.isAndroid) {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('New Band Name:'),
            content: TextField(
              controller: textEditingController,
            ),
            actions: [
              TextButton(
                child: const Text('Add'),
                onPressed: () => addNewBandToList(textEditingController.text),
              )
            ],
          );
        },
      );
    } else {
      showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title: const Text('New Band Name:'),
            content: CupertinoTextField(
              controller: textEditingController,
            ),
            actions: [
              CupertinoDialogAction(
                isDefaultAction: true,
                child: const Text('Add'),
                onPressed: () => addNewBandToList(textEditingController.text),
              ),
              CupertinoDialogAction(
                isDestructiveAction: true,
                child: const Text('Dismiss'),
                onPressed: () => Navigator.pop(context),
              ),
            ],
          );
        },
      );
    }
  }

  void addNewBandToList(String text) {
    if (text.trim().isEmpty) {
      return;
    }

    final _socketService = Provider.of<SocketService>(context, listen: false);
    _socketService.emit('add-band', {'name': text.trim()});

    Navigator.pop(context);
  }
}
