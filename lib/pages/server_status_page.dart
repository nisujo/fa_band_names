import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:band_names/services/socket_service.dart';

class ServerStatusPage extends StatelessWidget {
  const ServerStatusPage({Key? key}) : super(key: key);

  static const String route = 'server-status';

  @override
  Widget build(BuildContext context) {
    final _socketService = Provider.of<SocketService>(context);
    return Scaffold(
      body: Center(
        child: Text("${_socketService.serverStatus}"),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.message),
        onPressed: () {
          _socketService.emit('emit-message', {
            'name': 'Jose',
            'message': 'Hola desde Flutter.',
          });
        },
      ),
    );
  }
}
