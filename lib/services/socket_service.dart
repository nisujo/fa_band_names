import 'package:flutter/material.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

enum ServerStatus {
  online,
  offline,
  connecting,
}

class SocketService with ChangeNotifier {
  ServerStatus _serverStatus = ServerStatus.connecting;
  ServerStatus get serverStatus => _serverStatus;

  late IO.Socket _socket;
  IO.Socket get socket => _socket;
  Function get emit => _socket.emit;

  SocketService() {
    _initConfig();
  }

  void _initConfig() {
    const path = 'http://192.168.0.18:3000';
    // const path = 'https://flutter-bandnames.bunker.josefernandonieto.com';

    _socket = IO.io(
      path,
      IO.OptionBuilder()
          .setTransports(['websocket'])
          .enableAutoConnect()
          .build(),
    );

    _socket.onConnect((_) {
      _serverStatus = ServerStatus.online;
      notifyListeners();
    });
    _socket.onDisconnect((_) {
      _serverStatus = ServerStatus.offline;
      notifyListeners();
    });

    _socket.on('connect_error', (data) {
      print(data);
    });
    _socket.on('connect_timeout', (data) {
      print(data);
    });

    _socket.on('new-message', (data) {
      print(data);
    });
    _socket.on('emit-message', (data) {
      print(data);
    });
  }
}
