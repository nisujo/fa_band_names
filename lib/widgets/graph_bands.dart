import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:band_names/models/band.dart';

class GraphBands extends StatelessWidget {
  const GraphBands(
    this.bands, {
    Key? key,
  }) : super(key: key);

  final List<Band> bands;

  @override
  Widget build(BuildContext context) {
    Map<String, double> dataMap = {};

    for (Band band in bands) {
      dataMap["${band.name}"] = band.votes?.toDouble() ?? 0.0;
    }

    return PieChart(
      dataMap: dataMap,
      chartType: ChartType.ring,
      chartValuesOptions: const ChartValuesOptions(
        showChartValuesOutside: false,
        decimalPlaces: 1,
      ),
    );
  }
}
